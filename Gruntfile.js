module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            gruntfile: {
                src: 'Gruntfile.js'
            },
            validate: {
                src: '<%= pkg.main %>',
                options: {
                    laxcomma: true,
                    curly: true
                }
            }
        },
        watch: {
            jshintGruntfile: {
                files: 'jstoolkit.js',
                tasks: ['jshint:gruntfile'],
                options: {
                    atBegin: true
                }
            },
            jshintSrc: {
                files: '<%= pkg.main %>',
                tasks: ['jshint:validate'],
                options: {
                    atBegin: true
                }
            }
        },
        docco: {
            src: "<%= pkg.main %>",
            options: {
                output: 'docco_docs'
            }
        },
        uglify: {
            options: {
                report: 'gzip',
                banner: '/*!\n' +
                    ' * <%= pkg.main %> <%= pkg.version %>\n' +
                    ' * /\n' +
                    ' * (c) 2019-2020 MarJose Darang\n' +
                    ' * <%= pkg.main %> may be freely distributed under the MIT license.\n' +
                    '*/\n'
            },
            dist: {
                src: "<%= pkg.main %>",
                dest: "build/jstoolkit.min.js",
                options: {
                    sourceMap: true,
                    sourceMapName: 'build/jstoolkit.min.map'
                }
            },

        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-docco');

    grunt.registerTask('default', 'watch');
    grunt.registerTask('build', ['jshint:validate', 'uglify', 'docco']);
    grunt.registerTask('test', ['jshint']);
};
