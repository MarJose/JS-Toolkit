[![Documentation Status](https://readthedocs.org/projects/jstoolkit/badge/?version=latest)](https://jstoolkit.readthedocs.io/en/latest/?badge=latest)
[![pipeline status](https://gitlab.com/MarJose/JS-Toolkit/badges/master/pipeline.svg)](https://gitlab.com/MarJose/JS-Toolkit/commits/master)
# Get's Started!  
  `https://jstoolkit.readthedocs.io/en/latest/`
## What should i know?

> `jstoolkit.min.js` is a static namespace class.

>you must build first using the gradle.

> after you build using the Gradle `jstoolkit.min.js` will be placed inside the `Build` folder. 

## install 
>
```
npm i @marjose/jstoolkit
```  

## Usage
  
```javascript  
	<script type="text/javascript" src = 'jstoolkit.min.js' ></script>  
```  
  
## Debugging `Static`
> `Toolkit.Debugging`
  
- Console
   - log
      > The log console color is using the bootstrap `hex` color alert.
      - warning
          ```javascript
	        Example Syntax:
			Toolkit.Debugging.Console.log.warning('This is a warning debugging console with color');
			```
      - info
        ```javascript
	        Example Syntax:
			Toolkit.Debugging.Console.log.info('This is a info debugging console with color');
			```
      - success
          ```javascript
	        //Example Syntax:
			Toolkit.Debugging.Console.log.success('This is a success debugging console with color');
			```
      - danger
         ```javascript
	        /* Example Syntax: */
			toolkit.Debugging.Console.log.danger('This is a danger debugging console with color');
			```
   - Text
	   > The log console color is using the bootstrap `hex` color alert.
        - banner
            > `banner` is similar to the `log` but in different text format and styles.
            - [x] Danger
              ```javascript
                Example Syntax:
                toolkit.Debugging.Text.Banner.danger('This is a danger debugging console with color');
                ```
            - [x] Success
              ```javascript
                Example Syntax:
                toolkit.Debugging.Text.Banner.success('This is a success debugging console with color');
                ```
           - [x] Info
             ```javascript
                Example Syntax:
                toolkit.Debugging.Text.Banner.info('This is a info debugging console with color');
                ```
           - [x] Warning
             ```javascript
                Example Syntax:
                toolkit.Debugging.Text.Banner.warning('This is a danger debugging console with color');
                ```
   
  
## Interface `Static`

`Toolkit.Interface`

- DesignMode
	>This `feature` you can edit/modify your live web page design on real-time
	> Note: This `DesingMode` is just a temporary design. 
	- [x] On
		``` javascript
		Example Syntax:
		Toolkit.Interface.On();
		```  
	- [x] Off
		``` javascript
		Example Syntax:
		Toolkit.Interface.Off();
		```
## HTTP `Static` 
  >`Toolkit.HTTP`
  This will return a promises.
  
   - [x] GET 
	   >Note `header` variable/parameter must be `Key-Value`.
	   >headers = {"Accept": 'application/json'} 
	   ```javascript
           Example Syntax Call:
           toolkit.HTTP.GET(url, headers);
	   ```
  	    
   - [x] POST
	 > Note: `header` variable/parameter must be `Key-Value`.
	 >headers = {"Accept": 'application/json'} 
	 >Variable `data` is the data from `Form` submit.
	 ```javascript
         Example Syntax Call:
         toolkit.HTTP.POST(url, headers,data);
	 ```
	  
## Filter `Static`
- Currency
    - [x] Money  
    
      `Toolkit.Filter.Currency.Money(locales, CurrencySymbol,CurrencyDigitMoney)`  
      > A. Parameter: `locales `

       >First, you have the locales, this is the language and region settings. It is made up of language code and the country code. 
             ```
              language code + country code 
               ```
         Example:  
             >
            en-CA 
            en = English (language)
            CA = Canada (country) 
              
            en-Ph  
            en = English (language)
            Ph = Philippines (country)         
        
            Some More example  
            en-IN (English India) 
             fr-CA (French Canada) 
             ja-JP (Japanese Japan)  
                
        >B. Parameter: `CurrencySymbol`
    
        >Example Table
    
        Country | Alphabetic Code
        -------- | -----
        AUSTRIA | EUR
        AZERBAIJAN | AZN
        Philippines| PHP
        BAHRAIN | BHD
        BANGLADESH |BDT
        BARBADOS | BBD
        BELARUS | BYN
        BELGIUM | EUR
        BELIZE |BZD
        BENIN |XOF
        BERMUDA | BMD
        BHUTAN | INR
        BRAZIL | BRL
    
        >Here are some examples:
        -   CAD (Canadian dollar)
        -   USD (US dollar)
        -   EUR (Euro)
        -   CNY (Chinese RMB)
    
        > References:  [Current currency & funds code list](https://www.currency-iso.org/en/home/tables/table-a1.html)
        ```javascript
         let res =  toolkit.Currency.Money('en-Ph','PHP',1000)
      
          console.log(res);
      
         > Result:
          ₱1,000.00
          
      
        
        ```
- [x] isArray
	> `Toolkit.Filter.IsArray(values);`

	> This will return a `boolean` result.
	
- [x] isJsonObject
	> `Toolkit.Filter.isJsonObject(values);`

	> This will return a `boolean` result.
	
- [x] Unique
	> `Toolkit.Filter.Unique(jsonobject, comp);`
	
    > This `function` will remove the duplication data from the **JSON Object**
	```javascript
	$ > Example Code:
	
	const arrayObj = [  
	 { id: 1, name: "king" },
	 { id: 2, name: "master" },
	 { id: 3, name: "lisa" },
	 { id: 4, name: "ion" },
	 { id: 5, name: "jim" },
	 { id: 6, name: "gowtham" },
	 { id: 1, name: "jam" },
	 { id: 1, name: "lol" },
	 { id: 2, name: "kwick" },
	 { id: 3, name: "april" },
	 { id: 7, name: "sss" },
	 { id: 8, name: "brace" },
	 { id: 8, name: "peiter" },
	 { id: 5, name: "hey" },
	 { id: 6, name: "mkl" },
	 { id: 9, name: "melast" },
	 { id: 9, name: "imlast" },
	 { id: 10, name: "glow" } 
	 ];
	 let res = toolkit.Filter.Unique(arrayObj, 'id');
	 console.log(res);
	 
	 > Result:
	 
			Array(10)
			0:  {id:  1,  name:  "king"}
			1:  {id:  2,  name:  "master"}
			2:  {id:  3,  name:  "lisa"}
			3:  {id:  4,  name:  "ion"}
			4:  {id:  5,  name:  "jim"}
			5:  {id:  6,  name:  "gowtham"}
			6:  {id:  7,  name:  "sss"}
			7:  {id:  8,  name:  "brace"}
			8:  {id:  9,  name:  "melast"}
			9:  {id:  10,  name:  "glow"}
			length:  10
			__proto__:  Array(0)
	```
	```javascript
	Example Syntax Call:
	toolkit.Filter.Unique(arrayObj, comp);
	```
	

Building and Minify JS using Gradle
==

## instruction:
**Windows:**
> 1. install [Gradle](https://gradle.org/install/)
> 2.  then goto the project folder build and type: `gradle minifyjs` or `clean`

**Linux**
>1. install gradle by using `apt`. `sudo apt install gradle`
>2.  then goto the project folder build and type: `gradle minifyjs` or `clean`

Building jstoolkit.js using Gruntfile
---
### Requirements
  * [node.js](https://nodejs.org/) - Will probably work with most versions
  * [grunt-cli](http://gruntjs.com/using-the-cli)

### Build steps
  1. `git@gitlab.com:MarJose/JS-Toolkit.git`
  2. `cd JS-Toolkit`
  3. `npm install`
  4. `grunt build`

This will build  *jstoolkit.min.js*, *jstoolkit.min.map* and the *docs* folder.

### Continuous testing
You can run continuous testing that runs the tests on file changes by running
`grunt watch` or simply `grunt`.

If you want to just run the tests once you can do that by running `grunt test`.

Contributing
---
Before opening a pull request please make sure your changes follow the
[contribution guidelines](https://gitlab.com/MarJose/JS-Toolkit/blob/master/CONTRIBUTING.md).

#TODO
- [] Update Readme
- [] Create wiki Documentation
- [] Build