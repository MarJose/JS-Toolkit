/*
 *  jstoolkit.js
 * Copyright (c) 2019
 *  @Author MarJose Darang
 *  @License MIT
 *
 */

// jshint esversion: 6
(function (exports, module, define) {
    "use strict";

 /*   let jstoolkit = function (attribute, constraints, options){
        options = jstoolkit.extend({}, jstoolkit.options, options);
    };
*/


    let jstoolkit = function(selector){
        let ako = {};
        ako.selector = selector;
        ako.element = document.querySelector(ako.selector);

        // convert data to html and display
        // jstoolkit('#mydiv').attr('class', 'google').html();
        ako.html = function() {
            return ako.element;
        };


        // DOM attribute
        // jstoolkit('#mydiv').attr('class');
        // jstoolkit('#mydiv').attr('class','fa fa-smile').html(); -> you can only set the .html() function if the attr function value is not empty.
        ako.attr = function (name, value) {
            if (!value) {
                //get attribute data and display the value if existing
                return ako.element.getAttribute(name);
            } else {
                //set attribute data and display the value
                ako.element.setAttribute(name, value);
            }
            return ako;
        };

        // add/remove attribute on tags
        // you can use this inside on the event listener
        ako.toggleAttr = function(name){
            ako.element.toggleAttribute(name);
        };


        // event listener
        //jstoolkit('#btnsubmit').event('click', function (gg) {
        //    alert('you clicked ako!');
        // });
        ako.event = function (type, callback) {
            if (!toolkit.utilities.isFunction(callback)){
                throw new toolkit.debugging.console.log.Danger('Callback function should be a return function');
            }
            ako.element['on' + type] = callback;
            return ako;
        };

        //get the input text value
        //jstoolkit('#testinput').value()
        // `testinput` is the attribute ID for the textbox
        ako.value = function(string){
            if(string){
                let ret =  ako.element.value = string;
                return ret;
            }
            return ako.element.value;
        };

        return ako;
    };

    let toolkit = jstoolkit;

    toolkit.extend = function(obj) {
        [].slice.call(arguments, 1).forEach(function(source) {
            let attr;
            for (attr in source) {
                obj[attr] = source[attr];
            }
        });
        return obj;
    };


    toolkit.extend(jstoolkit,{
        version:{
            major: 1,
            minor: 0,
            patch: 0,
            metadata: "beta",
            toString: function () {
                let version = toolkit.utilities.format("%{major}.%{minor}.%{patch}", toolkit.version);
                if(!toolkit.utilities.isEmpty(toolkit.version.metadata)){
                    version += "+" + toolkit.version.metadata;
                }
                return version;
            }
        },
        //information
        info:{
            license: 'MIT',
            author: 'Marjose Darang',
            motto: 'If your doing it right, then you are not wrong!',
            copyright: '2019',
            toString: function () {
               return toolkit.utilities.format("License: %{license} \n author: %{author}\n Motto: %{motto}\n CopyRight: %{copyright}", toolkit.info);
            }
        },
        // lets rock the stone age!
        exposeModule: function(toolkit, root, exports, module, define) {
            if (exports) {
                if (module && module.exports) {
                    exports = module.exports = jstoolkit;
                }
                exports.jstoolkit = jstoolkit;
            } else {
                root.jstoolkit = jstoolkit;
                if (jstoolkit.utilities.isFunction(define) && define.amd) {
                    define([], function () { return validate; });
                }
            }
        },

    });

    // console interface debugging
    jstoolkit.debugging = {
        console:{
            log:{
                Warning: function (...string) {
                    return console.log('%c ' + string, 'color:#ffc107; font-weight: bold; ');
                },
                Info: function (...string) {
                    return console.log('%c ' + string, 'color:#5bc0de; font-weight: bold; font-size: 13px;');
                },
                Success: function (...string) {
                    return console.log('%c ' + string, 'color:#28a745; font-weight: bold; font-size: 13px;');
                },
                Danger: function (...string) {
                    return console.log('%c' + string, 'color:#dc3545; font-weight:bold; font-size: 13px;');
                }
            },
            text: {
                banner: {
                    Danger: function (...string) {
                        return console.log('%c' + string, 'background:#dc3545; color:white; font-weight:bold; font-size: 34px;');
                    },
                    Success: function (...string) {
                        return console.log('%c' + string, 'background:#28a745; color:white; font-weight:bold; font-size: 34px;');
                    },
                    Info: function (...string) {
                        return console.log('%c' + string, 'background:#5bc0de; color:white; font-weight:bold; font-size: 34px;');
                    },
                    Warning: function (...string) {
                        return console.log("%c" + string, 'background:#ffc107; color:white; font-weight:bold; font-size: 34px;');
                    }
                }
            }
        }
    };

    // for live webpage editing
    jstoolkit. interface = {
        designMode: {
            On: function () {
                document.body.contentEditable = 'true';
                document.designMode = 'on';
                toolkit.debugging.console.text.banner.Info('Design Mode is Enabled!');

            },
            Off: function () {
                document.body.contentEditable = 'false';
                document.designMode = 'off';
                toolkit.debugging.console.text.banner.Info('Design Mode is Disabled!');
            }
        }
    };

    // xhr method request
    jstoolkit.http = {
        /*
    * headers = {"Accept": 'application/json'}
    *  @ headers = Key-Value
    *  @FetchUrlData return promises
    * */
        GET: function (url, headers) {
            return new Promise(resolve => {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        resolve(xhttp.responseText);
                    }
                };
                xhttp.open("GET", url, true);
                xhttp.setRequestHeader(headers);
                xhttp.send(null);
            });
        },
        POST: function (url, headers, data) {
            return new Promise(resolve => {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        resolve(xhttp.responseText);
                    }
                };
                xhttp.open("POST", url, true);
                xhttp.setRequestHeader(headers);
                xhttp.send(data);
            });
        },
        PUT: function (url, headers, json) {
            /*eslint indent:0*/
            return new Promise(resolve => {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        resolve(xhttp.responseText);
                    }
                };
                xhttp.open("PUT", url, true);
                xhttp.setRequestHeader(headers);
                xhttp.send(json);
            });
        },
        DELETE: function (url, headers) {
            return new Promise(resolve => {
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        resolve(xhr.responseText);
                    }
                };
                xhr.open("DELETE", url, true);
                xhr.setRequestHeader(headers);
                xhr.send(null);
            });
        }

    };

     // utilities parts
    // usable for checking and verifying
    jstoolkit.utilities = {
        /*
        const arrayObj = [
              { id: 1, name: "king" },
              { id: 2, name: "master" },
              { id: 3, name: "lisa" },
              { id: 4, name: "ion" },
              { id: 5, name: "jim" },
              { id: 6, name: "gowtham" },
              { id: 1, name: "jam" },
              { id: 1, name: "lol" },
              { id: 2, name: "kwick" },
              { id: 3, name: "april" },
              { id: 7, name: "sss" },
              { id: 8, name: "brace" },
              { id: 8, name: "peiter" },
              { id: 5, name: "hey" },
              { id: 6, name: "mkl" },
              { id: 9, name: "melast" },
              { id: 9, name: "imlast" },
              { id: 10, name: "glow" }
            ];
            * Tooolkit.Filter.Unique(arrayObj,'id')
            * @return JSON Object
            ** @comp {unique key}
            */
        unique: function (arrayObj, comp) {
            return arrayObj
                .map(e => e[comp])
                // store the keys of the unique objects
                .map((e, i, final) => final.indexOf(e) === i && i)

                // eliminate the dead keys & store unique objects
                .filter(e => arrayObj[e]).map(e => arrayObj[e]);


        },
        format: toolkit.extend(function(str, vals) {
            if (!toolkit.utilities.isString(str)) {
                return str;
            }
            return str.replace(toolkit.utilities.format.FORMAT_REGEXP, function(m0, m1, m2) {
                if (m1 === '%') {
                    return "%{" + m2 + "}";
                } else {
                    return String(vals[m2]);
                }
            });
        }, {
            // Finds %{key} style patterns in the given string
            FORMAT_REGEXP: /(%?)%\{([^\}]+)\}/g
        }),
        /*
        * @return {Boolean}
        * */
        isArray: function (value) {
            return {}.toString.call(value) === '[object Array]';
        },
        /*
        * @return {Boolean}
        * */
        isJsonObject: function (value) {
            return value && typeof value === 'object' && value.constructor === Object;
        },
        isNumber: function (value) {
            return typeof value === 'number' && !isNaN(value);
        },
        // Returns false if the object is not a function
        isFunction: function (value) {
            return typeof value === 'function';
        },

        // A simple check to verify that the value is an integer. Uses `isNumber`
        // and a simple modulo check.
        isInteger: function (value) {
            return toolkit.utilities.isNumber(value) && value % 1 === 0;
        },

        // Checks if the value is a boolean
        isBoolean: function (value) {
            return typeof value === 'boolean';
        },

        // Uses the `Object` function to check if the given argument is an object.
        isObject: function (obj) {
            return obj === Object(obj);
        },
        // Simply checks if the object is an instance of a date
        isDate: function (obj) {
            return obj instanceof Date;
        },

        // Returns false if the object is `null` of `undefined`
        isDefined: function (obj) {
            return obj !== null && obj !== undefined;
        },

        // Checks if the given argument is a promise. Anything with a `then`
        // function is considered a promise.
        isPromise: function (p) {
            return !!p && toolkit.utilities.isFunction(p.then);
        },

        isJqueryElement: function (o) {
            return o && toolkit.utilities.isString(o.jquery);
        },

        isDomElement: function (o) {
            if (!o) {
                return false;
            }
            if (!o.querySelectorAll || !o.querySelector) {
                return false;
            }
            if (toolkit.utilities.isObject(document) && o === document) {
                return true;
            }

            // http://stackoverflow.com/a/384380/699304
            /* istanbul ignore else */
            if (typeof HTMLElement === "object") {
                return o instanceof HTMLElement;
            } else {
                return o &&
                    typeof o === "object" && true && o.nodeType === 1 &&
                    typeof o.nodeName === "string";
            }
        },
        EMPTY_STRING_REGEXP: /^\s*$/,

            isEmpty: function (value) {
            // Null and undefined are empty
            if (!toolkit.utilities.isDefined(value)) {
                return true;
            }

            // functions are non empty
            if (toolkit.utilities.isFunction(value)) {
                return false;
            }

            // Whitespace only strings are empty
            if (toolkit.utilities.isString(value)) {
                return toolkit.utilities.EMPTY_STRING_REGEXP.test(value);
            }

            // For arrays we use the length property
            if (toolkit.utilities.isArray(value)) {
                return value.length === 0;
            }

            // Dates have no attributes but aren't empty
            if (toolkit.utilities.isDate(value)) {
                return false;
            }

            // If we find at least one property we consider it non empty
            if (toolkit.utilities.isObject(value)) {
                value.forEach(() => {
                    return false;
                });
                return true;
            }

            return false;
        },
        // "Prettifies" the given string.
        // Prettifying means replacing [.\_-] with spaces as well as splitting
        // camel case words.
        prettify: function (str) {
            if (toolkit.utilities.isNumber(str)) {
                // If there are more than 2 decimals round it to two
                if ((str * 100) % 1 === 0) {
                    return "" + str;
                } else {
                    return parseFloat(Math.round(str * 100) / 100).toFixed(2);
                }
            }

            if (toolkit.utilities.isArray(str)) {
                return str.map(function (s) {
                    return toolkit.utilities.prettify(s);
                }).join(", ");
            }

            if (toolkit.utilities.isObject(str)) {
                if (!toolkit.utilities.isDefined(str.toString)) {
                    return JSON.stringify(str);
                }

                return str.toString();
            }

            // Ensure the string is actually a string
            str = "" + str;

            return str
            // Splits keys separated by periods
                .replace(/([^\s])\.([^\s])/g, '$1 $2')
                // Removes backslashes
                .replace(/\\+/g, '')
                // Replaces - and - with space
                .replace(/[_-]/g, ' ')
                // Splits camel cased words
                .replace(/([a-z])([A-Z])/g, function (m0, m1, m2) {
                    return "" + m1 + "" + m2.toLowerCase();
                })
                .toLowerCase();
        },
        isString: function (value) {
            return typeof value === 'string';

        }

    };

    //validators parts
    jstoolkit.validators = {
        presence: function(value, options) {
            options = toolkit.extend({}, this.options, options);
            if (options.allowEmpty !== false ? !toolkit.utilities.isDefined(value) : toolkit.utilities.isEmpty(value)) {
                return options.message || this.message || "can't be blank";
            }
        },
        format: function(value, options) {
            if (toolkit.utilities.isString(options) || (options instanceof RegExp)) {
                options = {pattern: options};
            }

            options = toolkit.extend({}, this.options, options);

            let message, pattern, match;
            message = options.message || this.message || "invalid";
            pattern = options.pattern;

            // Empty values are allowed
            if (!toolkit.utilities.isDefined(value)) {
                return;
            }
            if (!toolkit.utilities.isString(value)) {
                return message;
            }

            if (toolkit.utilities.isString(pattern)) {
                pattern = new RegExp(options.pattern, options.flags);
            }
            match = pattern.exec(value);
            if (!match || match[0].length !== value.length) {
                return message;
            }
        }

    };

    //related to the finance/banking
    jstoolkit.finance = {
        /**
         * @return {string}
         */
        Money: function (locales, CurrencySymbol, CurrencyDigitMoney) {
            const formatter = new Intl.NumberFormat(locales, {
                style: 'currency',
                currency: CurrencySymbol,
                minimumFractionDigits: 2
            });
            return formatter.format(CurrencyDigitMoney);

        },
        //Calculate the Financed Payment Amount for every month
        /**
         * @return {number}
         */
        CalculatePayment: function (finAmount, finMonths, finInterest) {
            let result = 0;
            if (finInterest === 0) {
                result = finAmount / finMonths;
            } else {
                let i = ((finInterest / 100) / 12),
                    i_to_m = Math.pow((i + 1), finMonths),
                    p = finAmount * ((i * i_to_m) / (i_to_m - 1));
                result = Math.round(p * 100) / 100;
            }
            return result;
        }
    };

    // Form validation
    jstoolkit.form = {
        // jstoolkit.form.Validate('#myFormId', [options]);
        Validate: function (formId, options) {
            $(formId).validate(options);
        }
    };


    // for npm use
    jstoolkit.exposeModule(jstoolkit, this, exports, module, define);
}).call(this,typeof exports !== 'undefined' ? exports : null,
    typeof module !== 'undefined' ? /* istanbul ignore next */ module : null,
    typeof define !== 'undefined' ? /* istanbul ignore next */ define : null);